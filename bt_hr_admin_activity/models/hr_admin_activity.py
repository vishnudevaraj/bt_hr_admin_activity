# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2018 BroadTech IT Solutions Pvt Ltd 
#    (<http://broadtech-innovations.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import api, fields, models, _
from openerp.exceptions import ValidationError
import datetime

class HrAdminActivity(models.Model):
    _name = "hr.admin.activity"
    _description = "HR Activites"
    _inherit = ['mail.thread']

    name = fields.Char(string='Activity',track_visibility='onchange')
    type = fields.Selection([('hr','HR'),('admin','Admin')],string='Type',copy=False)
    defined_date = fields.Date(string='Defined Date',default=datetime.date.today())
    deadline_date = fields.Date(string='Deadline',track_visibility='onchange',copy=False)
    description = fields.Text('Description',copy=False)
    state = fields.Selection([('draft','Draft'),('open','Open'),('in_progress','In Progress'),('cancelled','Cancelled'),
                             ('hold','Hold'),('done','Done')],string='State',copy=False,default='draft')
    responsible_id = fields.Many2one('res.users','Responsible',default=lambda self: self.env.user)
    
    @api.model
    def create(self, vals):
        res = super(HrAdminActivity, self).create(vals)
        if vals.get('state') == 'draft' and vals.get('responsible_id',False):
            responsible_user = vals.get('responsible_id')
            partner = self.env['res.users'].browse(responsible_user).partner_id.id
            res.message_subscribe([partner])
        print '------------------------------------------',vals.get('defined_date'),vals.get('deadline_date')
        if vals.get('deadline_date',False):
            if vals.get('deadline_date',False) <= vals.get('defined_date',False):
                raise ValidationError(_("The Deadline must be anterior to the Defined Date."))
        return res
    
    @api.multi
    def write(self, vals):
        res = super(HrAdminActivity, self).write(vals)
        if self.state == 'draft' and vals.get('responsible_id',False):
            responsible_user = vals.get('responsible_id')
            partner = self.env['res.users'].browse(responsible_user).partner_id.id
            self.message_subscribe([partner])
            print '------------------------------------------',self.deadline_date,vals.get('deadline_date'),vals.get('defined_date')
        if self.deadline_date:
            if self.deadline_date <= self.defined_date:
                raise ValidationError(_("The Deadline must be anterior to the Defined Date."))
        return res
    
    
    
    
# vim:expandtab:smartindent:tabstop=2:softtabstop=2:shiftwidth=2:
